import 'package:flutter/material.dart';




Color getColorFromHex(String hexColor) {
  hexColor = hexColor.replaceAll("#", "");
  if (hexColor.length == 6) {
    hexColor = "FF" + hexColor;
  }
  if (hexColor.length == 8) {
    return Color(int.parse("0x$hexColor"));
  }
  else{return null;}
}



Widget contbutton({String text,Color color=Colors.transparent,Color txtcolor= Colors.white70,Icon icondata}) {
  return Container(
    decoration: BoxDecoration(
      color: color,
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color:getColorFromHex('#3F6F9F'))),
    width: 260,
    height: 40,
    child:icondata!=null? Padding(
      padding: const EdgeInsets.only(left: 30),
      child: Row(
        children: <Widget>[
          IconButton(icon: icondata,color: getColorFromHex('#3F6F9F'),onPressed: (){},),
          Text(
            text,
            style: TextStyle(color:txtcolor),
          ),
        ],
      ),
    ): Center(
            child: Text(
          text,
          style: TextStyle(color:txtcolor),
        )),
  );
}
