import 'package:graduation/screens/add_new_case.dart';
import 'package:graduation/screens/health_state.dart';
import 'package:graduation/screens/homepage.dart';
import '../screens/prevention.dart';
import '../screens/scan2.dart';
import '../widgets/textField.dart';
import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';

class BottomNavBar extends StatefulWidget {
  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  int pageIndex = 0;
  GlobalKey _bottomNavigationKey = GlobalKey();
  final Prevention _prevention = new Prevention();
  final HomePage _homePage = new HomePage();
  final HealthState _healthState = new HealthState();
  final AddNewCase _addNewCase = new AddNewCase();
  final Scan _scan = new Scan();
  Widget _showpage = new HomePage();

  Widget _pageChooser(int page) {
    switch (page) {
      case 0:
        return _homePage;
        break;

      case 1:
        return _prevention;
        break;

      case 2:
        return _scan;
        break;

      case 3:
        return _addNewCase;
        break;
      case 4:
        return _healthState;
        break;
      default:
        return new Container(
          child: Text("No page found by page chooser"),
        );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: CurvedNavigationBar(
          key: _bottomNavigationKey,
          index: pageIndex,
          height: 50.0,
          items: <Widget>[
            Icon(Icons.home, size: 30),
            Icon(Icons.local_hospital, size: 30),
            Icon(Icons.camera_alt, size: 30),
            Icon(Icons.add, size: 30),
            Icon(Icons.help, size: 30),
          ],
          color: Colors.white,
          buttonBackgroundColor: Colors.white,
          backgroundColor: getColorFromHex('#FF3F6F9F'),
          animationCurve: Curves.easeInOut,
          animationDuration: Duration(milliseconds: 600),
          onTap: (int tappedIndex) {
            setState(() {
              _showpage = _pageChooser(tappedIndex);
            });
          },
        ),
        body: _showpage);
  }
}
