import 'package:flutter/material.dart';

class TextForm extends StatelessWidget {
  TextForm(
      {@required this.controller,
      this.hinttext,
      this.maxNum,
      this.onsaved,
      this.prefixicon,
      this.suffixicon,
      this.suffixiconcolor,
      this.suffixiconfunction,
      this.textInputType});

  final TextEditingController controller;
  final int maxNum;
  final String hinttext;
  final IconData prefixicon;
  final IconData suffixicon;
  final Color suffixiconcolor;
  final Function suffixiconfunction;
  final TextInputType textInputType;
final Function onsaved;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 25.0, left: 25.0),
      child: Container(
        height: 50.0,
        child: TextFormField(
          keyboardType: textInputType,
          maxLength: maxNum,
          onSaved: onsaved,
          controller: controller,
          decoration: InputDecoration(
              labelText: hinttext,
              prefixIcon: Icon(prefixicon),
              suffixIcon: IconButton(
                icon: Icon(
                  suffixicon,
                  color: suffixiconcolor,
                ),
                onPressed: suffixiconfunction,
              ),
              border: OutlineInputBorder(
                  borderRadius: (BorderRadius.all(Radius.circular(15.0)))),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(
                    15.0,
                  )),
                  borderSide: BorderSide(color: getColorFromHex('#FF3F6F9F')))),
        ),
      ),
    );
  }
}

Color getColorFromHex(String hexColor) {
  hexColor = hexColor.replaceAll("#", "");
  if (hexColor.length == 6) {
    hexColor = "FF" + hexColor;
  }
  if (hexColor.length == 8) {
    return Color(int.parse("0x$hexColor"));
  }
  else{
    return null;
  }
  
}
