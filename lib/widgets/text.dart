import '../widgets/button.dart';
import 'package:flutter/material.dart';
class textField extends StatefulWidget {

  final String hintText;
  final Icon icon;
  final String initialvalue;
  final TextInputType textInputType;
  bool t;
  final Function onChange;
  textField({this.initialvalue, this.hintText, this.icon, this.textInputType, this.onChange,this.t =false});
  @override
  _textFieldState createState() => _textFieldState();
}

class _textFieldState extends State<textField> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 30),
        child: TextFormField(
            obscureText: widget.t,
            initialValue: widget.initialvalue,
            keyboardType: widget.textInputType,
            onChanged: widget.onChange,
            validator: (val) {
              if (val.isEmpty) {
                return '* this field is requried';
              }
              return null;
            },
            decoration: InputDecoration(

              prefixIcon: widget.icon,
              contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              hintText: widget.hintText,
              hintStyle: TextStyle(color: Colors.black26),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: getColorFromHex('#3F6F9F')),
              ),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: getColorFromHex('#3F6F9F'))),
            )));
  }

}
class EditInfo extends StatelessWidget {
  final String initialvalue;
  final String hinttext;
  EditInfo({this.hinttext,this.initialvalue});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: TextFormField(
        textAlign: TextAlign.end,
        initialValue: initialvalue,
        cursorColor: getColorFromHex('#3F6F9F'),
        decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            border: InputBorder.none,
            prefixText: hinttext,
            hintStyle:
            TextStyle(color: getColorFromHex('#3F6F9F'), fontSize: 15)),
      ),
    );
  }
}

