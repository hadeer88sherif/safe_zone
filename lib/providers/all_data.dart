import 'package:flutter/cupertino.dart';
import 'package:http/http.dart'as http;
import 'dart:convert';

class AllData with ChangeNotifier {
  Map _data ={};

  Map get worldData {
    return _data;
  }
  Future<void> fetchAndSetCountriesData() async {
    const allUrl = 'https://corona.lmao.ninja/v2/all';
    http.Response responseCountry =await http.get(allUrl);
    _data =json.decode(responseCountry.body);
    notifyListeners();
  }
}
