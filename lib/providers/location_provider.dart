import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class LocationItem {
  final String id;
  final String title;
  final String token;
  final double lat;
  final double long;
  LocationItem({this.token,@required this.lat, @required this.long, @required this.title, this.id});
}
class MarketPlace with ChangeNotifier{

  List<LocationItem>_zones=[];

  List<LocationItem>get zones{
    return _zones;
  }


  //location edit

  Future<void> updateLocation(String id, LocationItem newmarket) async {
    final marketIndex = _zones.indexWhere((marketplace) => marketplace.id == id);
    if (marketIndex >= 0) {
      final url = 'https://flutter-update.firebaseio.com/market/$id.json';
      await http.patch(url,
          body: json.encode({
            'p_ssn':newmarket.lat + newmarket.long ,
            'longitude':newmarket.long,
            'latidude':newmarket.lat,
            'title':newmarket.title,
            'token':newmarket.token,
           
          }));
      _zones[marketIndex] = newmarket;
      notifyListeners();
    } else {
      print('...');
    }
  }



//new market
Future<void> newMarket(LocationItem newmarket) async {
    const url = 'https://flutter-update.firebaseio.com/market.json';
    try {
      final response = await http.post(
        url,
        body: json.encode({
          'p_ssn':newmarket.lat + newmarket.long ,
            'longitude':newmarket.long,
            'latidude':newmarket.lat,
            'title':newmarket.title,
            'token':newmarket.token,

         
        }),
      );
      final newzone = LocationItem(
         id:"${newmarket.lat+newmarket.long}",
 title:newmarket.title,
   token:newmarket.token,
   lat:newmarket.lat,
   long:newmarket.long,
          
        
        
      );
      _zones.add(newzone);
      
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }






  void addMarket(LocationItem loc){
    _zones.insert(0,
      LocationItem(title: loc.title,lat: loc.lat,long: loc.long,id: "${loc.lat+loc.long}",token: loc.token)
    );
    notifyListeners();
  }

  void updateMarket(LocationItem loc){
    _zones.insert(0,
        LocationItem(title: loc.title,lat: loc.lat,long: loc.long,id: "${loc.lat+loc.long}",token: loc.token)
    );
    notifyListeners();
  }

LocationItem findbyid(String title){
  return _zones.firstWhere((loc)=>loc.title==title);

}


}