import 'package:flutter/material.dart';

final kTitleStyle = TextStyle(
  color: Color(0xFF3F6F9F),
  fontFamily: 'CM Sans Serif',
  fontSize: 26.0,
  height: 1.5,
  fontWeight: FontWeight.w700,
);

final kSubtitleStyle = TextStyle(
    color: Color(0xFF3F6F9F),
    fontSize: 21.0,
    height: 1.3,
    wordSpacing: 3,
);