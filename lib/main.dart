

import 'package:graduation/providers/all_data.dart';
import 'package:graduation/providers/location_provider.dart';

import 'package:graduation/screens/edit.dart';





import './widgets/textField.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './screens/onboarding_screen.dart';


void main(){
  // WidgetsFlutterBinding.ensureInitialized();
  // SharedPreferences prefs = await SharedPreferences.getInstance();
  // bool seen=prefs.getBool('seen');
  // Widget _screen ;
  // if(seen==null||seen==false){
    
  //   _screen=OnboardingScreen();
  // }else{
  //   _screen=Splash();
  // }
   runApp(MyApp());

}

class MyApp extends StatelessWidget {
  // final Widget _screen;
  // MyApp(this._screen);

  @override
  Widget build(BuildContext context) {
     return MultiProvider(
        providers: [

        ChangeNotifierProvider.value(
        value: AllData(),
    ),
   
   
  ChangeNotifierProvider.value(
      value: MarketPlace())
    ],
      child: MaterialApp(
        title: 'Safe Zone',
         debugShowCheckedModeBanner: false,
        theme: ThemeData(

       accentColor: getColorFromHex('#3F6F9F'),primaryColor: getColorFromHex('#3F6F9F')
        ),
      
        home:OnboardingScreen()
        ,routes: {
          Edit.routname:(ctx)=>Edit()
        },
      )
     );
  }
}
