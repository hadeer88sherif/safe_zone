import '../screens/verfication.dart';
import '../widgets/logo.dart';
import '../widgets/text.dart';
import '../widgets/button.dart';
import 'package:flutter/material.dart';

class EditNum extends StatefulWidget {
  @override
  _EditNumState createState() => _EditNumState();
}

class _EditNumState extends State<EditNum> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
      
        
            child: 
              
              Column(
                children: <Widget>[
                   Padding(
                 padding:  EdgeInsets.only(left: 20, top: 70),
                 child: logo(context),
               ),
                  Padding(
                    padding: const EdgeInsets.only(top: 50),
                    child: Text('Edit Mobile Number',
                        style: TextStyle(
                          fontSize: 25,
                          color: Theme.of(context).accentColor,
                          fontWeight: FontWeight.bold,
                        )),
                  ),
                  
                  Padding(
                    padding: const EdgeInsets.only(top: 50),
                    child: Text('Please Insert Your Correct Mobile Number',
                        style: TextStyle(
                            fontSize: 16, color: Theme.of(context).accentColor)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 50),
                    child: textField(
                        hintText: 'Mobile Number',
                        icon: Icon(
                          Icons.call,
                          color: Colors.black26,
                        )),
                  ),
                  
                  Padding(
                    padding: const EdgeInsets.only(top: 50),
                    child: InkWell(
                      child: contbutton(
                          text: 'Submit', color: Theme.of(context).accentColor),
                      onTap: () {
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (ctx) => Verfication()));
                      },
                    ),
                  ),
                ],
              )
            
,
      ),
    );
  }
}
