
import 'package:flutter/material.dart';
import 'package:graduation/screens/edit.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:provider/provider.dart';
import 'package:graduation/providers/location_provider.dart';

class Qr extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final qrData = Provider.of<MarketPlace>(context, listen: false).zones;
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.edit),
              onPressed: () {
                Navigator.of(context)
                    .pushNamed(Edit.routname);
              })
        ],
        title: Text('Qr Code'),
        backgroundColor: Color(0xFF3F6F9F),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 30, right: 30),
            child: Container(
              alignment: Alignment.center,
              height: MediaQuery.of(context).size.height * .4,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(
                  30,
                )),
              ),
              child: QrImage(
                data:
                    "title= ${qrData[0].title}latitude= ${qrData[0].lat.toString()}longtiude= ${qrData[0].long.toString()}",
                version: QrVersions.auto,
                size: 250,
                foregroundColor: Color(0xFF3F6F9F),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
