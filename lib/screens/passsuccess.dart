import '../screens/location.dart';
import '../widgets/button.dart';
import 'package:flutter/material.dart';

class PassSuccess extends StatefulWidget {
  @override
  _PassSuccessState createState() => _PassSuccessState();
}

class _PassSuccessState extends State<PassSuccess> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: 
              ListView(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                    
                      Padding(
                        padding: const EdgeInsets.only(top: 100,bottom: 80),
                        child: Container(
                            width: 120,
                            height: 120,
                            child: Image.asset('assets/checked.png',
                                fit: BoxFit.fill)),
                      ),
                      
                      Text(
                        'Your New Password Has Been\n       Updated Successflly',
                        style: TextStyle(
                            fontSize: 25, color: Theme.of(context).accentColor),
                      ),
                      
                      Padding(
                        padding: const EdgeInsets.only(top: 170),
                        child: InkWell(
                          child: contbutton(
                              text: 'Continue',
                              txtcolor: Colors.white70,
                              color:Theme.of(context).accentColor),
                          onTap: () {
                            Navigator.of(context).push(
                                MaterialPageRoute(builder: (ctx) => Locations()));
                          },
                        ),
                      )
                    ],
                  ),
                ],
              )
          
      )
    ;
  }
}
