

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:graduation/screens/register.dart';
import '../utilities/styles.dart';
import '../widgets/myheader.dart';




class OnboardingScreen extends StatefulWidget {
  @override
  _OnboardingScreenState createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  final controller = ScrollController();
  double offset = 0;
  final int _numPages = 3;
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;

  List<Widget> _buildPageIndicator() {
    List<Widget> list = [];
    for (int i = 0; i < _numPages; i++) {
      list.add(i == _currentPage ? _indicator(true) : _indicator(false));
    }
    return list;
  }

  @override
  void initState() {
  
    super.initState();
    controller.addListener(onScroll);
  }

  @override
  void dispose() {
   
    controller.dispose();
    super.dispose();
  }

  void onScroll() {
    setState(() {
      offset = (controller.hasClients) ? controller.offset : 0;
    });
  }

  Widget _indicator(bool isActive) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(horizontal: 8.0),
      height: isActive ? 15.0 : 10.0,
      width: isActive ? 15.0 : 10.0,
      decoration: BoxDecoration(
        color: isActive ? Color(0xFF3F6F9F) : Colors.blue[100],
        borderRadius: BorderRadius.all(Radius.circular(600)),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: Container(
          color: Color(0xFFB7B7B7).withOpacity(.16),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 0.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  child: Container(
                    child: PageView(
                      physics: ClampingScrollPhysics(),
                      controller: _pageController,
                      onPageChanged: (int page) {
                        setState(() {
                          _currentPage = page;
                        });
                      },
                      children: <Widget>[
                        SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              MyHeader(
                                image: "assets/girldoctor.png",
                                offset: offset,
                              ),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * .04),
                              Padding(
                                padding:
                                    const EdgeInsets.only(right: 30, left: 30),
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      'Welcome',
                                      style: kTitleStyle,
                                      textAlign: TextAlign.center,
                                    ),
                                    SizedBox(
                                        height:
                                            MediaQuery.of(context).size.height *
                                                .02),
                                    Text(
                                      'Track COVID-19 local and global coronavirus cases with active, recoveries and death rate on the map',
                                      style: kSubtitleStyle,
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              MyHeader(
                                image: "assets/user.png",
                                offset: offset,
                              ),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * .04),
                              Padding(
                                padding:
                                    const EdgeInsets.only(right: 30, left: 30),
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      'For User',
                                      style: kTitleStyle,
                                      textAlign: TextAlign.center,
                                    ),
                                    SizedBox(
                                        height:
                                            MediaQuery.of(context).size.height *
                                                .02),
                                    Text(
                                      'Social Monitoring app will track people who already tested positive for Covid-19'
                                      ' then will send an alert to other people they have recently come into close contact with them to go into quarantine or take other protective measures.',
                                      style: kSubtitleStyle,
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              MyHeader(
                                image: "assets/location.png",
                                offset: offset,
                              ),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * .04),
                              Padding(
                                padding:
                                    const EdgeInsets.only(right: 30, left: 30),
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      'For Location',
                                      style: kTitleStyle,
                                      textAlign: TextAlign.center,
                                    ),
                                    SizedBox(
                                        height:
                                            MediaQuery.of(context).size.height *
                                                .02),
                                    Text(
                                      'The software will make qr code for market places '
                                      'Then it will be scanned by users to manage user visits history.'
                                      ' Alert market place with cases that visit it for sterilization',
                                      style: kSubtitleStyle,
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                _currentPage != _numPages
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Divider(
                            color: Colors.grey.shade50,
                            thickness: 3,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              width: double.infinity,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  FlatButton(
                                    onPressed: () {
                                      Navigator.pushReplacement(context,
                                          MaterialPageRoute(builder: (context) {
                                        // _updateSeen();
                                        return Register();
                                      }));
                                    },
                                    child: Text(
                                      "SKIP",
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.grey),
                                    ),
                                  ),
                                  Row(
                                    children: _buildPageIndicator(),
                                  ),
                                  FlatButton(
                                    onPressed: () {
                                      if (_currentPage == _numPages - 1) {
                                        Navigator.pushReplacement(context,
                                            MaterialPageRoute(
                                                builder: (context) {
                                          // _updateSeen();
                                          return Register();
                                        }));
                                      } else {
                                        _pageController.nextPage(
                                          duration: Duration(milliseconds: 500),
                                          curve: Curves.ease,
                                        );
                                      }
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Text(
                                          'Next',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Color(0xFF3F6F9F),
                                            fontSize: 20.0,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      )
                    : Text(''),
              ],
            ),
          ),
        ),
      ),

    );
  }

  // void _updateSeen() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   prefs.setBool("seen", true);
  // }
}
