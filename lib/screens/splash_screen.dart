import 'dart:async';

import '../widgets/buttom_nav_bar.dart';
import 'package:flutter/material.dart';


class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 3), () {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => BottomNavBar(),
          ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[200],
        body: Center(
            child: Container(
                width: MediaQuery.of(context).size.width*.6,
                height: MediaQuery.of(context).size.height*.5,
                child: Image.asset('assets/logo.png'))));
  }
}
