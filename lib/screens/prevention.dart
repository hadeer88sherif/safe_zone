import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import '../utilities/constant.dart';
class Prevention extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: prefix0.Colors.grey[200],
      body: ListView(
        padding: const EdgeInsets.all(8),
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 35,right: 20,bottom: 20,left: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("covid-19".toUpperCase(),style: TextStyle(fontSize: 20,fontWeight: FontWeight.w500,color: Color(0xFF3F6F9F)),textAlign: TextAlign.center,),
                SizedBox(height: 5,),
                Text('Symptoms',style: TextStyle(fontSize: 30,fontWeight: FontWeight.w700,color: Color(0xFF3F6F9F)),textAlign: TextAlign.center,)
              ],
            ),
          ),
          Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 10,right: 10,top: 10, bottom: 10),
                child: Row(
                  children: <Widget>[
                    Symptoms(
                      image: 'assets/fever.png',
                      title: 'Fever',
                    ),
                    Symptoms(
                      image: 'assets/chest.png',
                      title: 'Chest Pain',
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 10,right: 10,),
                child: Row(
                  children: <Widget>[
                    Symptoms(
                      image: 'assets/noise.png',
                      title: 'Runny Nose',
                    ),
                    Symptoms(
                      image: 'assets/cough.png',
                      title: 'Dry Cough',
                    )
                  ],
                ),
              ),
            ],
          ),
          SizedBox( height: MediaQuery.of(context).size.height*.1, child: Divider(color: prefix0.Colors.grey[100],height: 100,indent: 20,endIndent: 20,thickness: 2,),),
          Container(
            padding: EdgeInsets.only(right: 20,bottom: 20,left: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Prevention',style: TextStyle(fontSize: 30,fontWeight: FontWeight.w700,color: Color(0xFF3F6F9F)),textAlign: TextAlign.center,),
                prefix0.SizedBox(height: 5,)
                ,Container(child: prefix0.Image.asset('assets/maledoctor.png'),)
              ],
            ),
          ),
          Prevent(
            mainTitle: "sneezing/coughing etiquette",
            subTitle: "Cover your mouth when you cough or sneeze,"
                "with a tissue or the inside of your elbow",
            image: 'assets/kmama.png',
          ),
          Prevent(
            mainTitle: "wash your hands",
            subTitle: "Wash them often, with water and lots of 20 secods",
            image: 'assets/hand.png',
          ),
          Prevent(
            mainTitle: "eyes,nose,mouth",
            subTitle: "Hands touch many surfaces and can pick up viruses. "
                "Avoid toucching your eyes, nose or mouth. The virus can enter your body and  can make you sick ",
            image: 'assets/dontTouch.png',
          ),
          Prevent(
            mainTitle: "if you are sick",
            subTitle: "If you hava a flu-like illness, inform the people around you. "
                "if your illness is not mild, seek medical care.",
            image: 'assets/doctor.png',
          ),
          SizedBox( height: 10),

        ],
      ),
    );
  }
}

class Symptoms extends StatelessWidget {
  Symptoms({@required this.title, this.image});
  final String title;
  final String image;
  @override
  Widget build(BuildContext context) {
    return Expanded(child: Container(
      height: 150,
      margin: EdgeInsets.only(right: 5,left: 5,),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(color: kBackgroundColor,blurRadius: 0, offset: Offset(0 ,2))
          ]
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            height: 100,
              width: 120,
              child: Image.asset(image,)),
          prefix0.SizedBox(height: 10,),
          Text(title,style: TextStyle(fontSize: 18,fontWeight: FontWeight.w700,color: Color(0xFF3F6F9F)),textAlign: TextAlign.center,)
        ],
      ),
    ));
  }
}

class Prevent extends StatelessWidget {
 Prevent({@required this.mainTitle, this.subTitle, this.image});
 final String mainTitle;
 final String subTitle;
 final String image;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 20,left: 20,top: 10,),
      height:  160,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(color: kBackgroundColor,blurRadius: 0, offset: Offset(0 ,2))
          ]
      ),
      child: Row(
        children: <Widget>[
          Container(
            height: 120,
            width: 120,
            child: Image.asset(image),
          ),
          Expanded(child: Container(
            margin: EdgeInsets.only(top: 20,right: 10),
            child: Column(
              children: <Widget>[
                Text(mainTitle.toUpperCase(),style: TextStyle(fontSize: 20,fontWeight: FontWeight.w700,color: Color(0xFF3F6F9F)),textAlign: TextAlign.center,),
                SizedBox(height: 5,),
                Text(subTitle,
                  style: TextStyle(fontWeight: FontWeight.w500,fontSize: 14,),textAlign: TextAlign.center,),
              ],
            ),
          ))
        ],
      ),
    );
  }
}
