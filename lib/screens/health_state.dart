import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class HealthState extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Health Status"),
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 100),
            child: Center(
              child: QrImage(
                data: "health state",
                version: QrVersions.auto,
                size: 250,
                foregroundColor: Colors.green[600],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 30,right: 10,left: 10),
            child: Text(
                "Health code color holds your encryption personal data to maintain your privacy. the color coding reflects your status.",style: TextStyle(
              fontSize: 18,fontWeight: FontWeight.w600,color: Color(0xFF3F6F9F),
            ),textAlign: TextAlign.center,),
          ),
          SizedBox(height: MediaQuery.of(context).size.height*.02,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30,vertical: 20),
            child: Row(
              children: <Widget>[
                Container(width: 15,height: 15,decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.green
                ),),
                SizedBox(width: MediaQuery.of(context).size.width*.03,),
                Text("Green - Good Health",style: TextStyle(color: Colors.green,fontSize: 20,fontWeight: FontWeight.w400),),
              ],
            ),
          ),
          // Padding(
          //   padding: const EdgeInsets.symmetric(horizontal: 30),
          //   child: Row(
          //     children: <Widget>[
          //       Container(width: 15,height: 15,decoration: BoxDecoration(
          //           borderRadius: BorderRadius.circular(15),
          //           color: Colors.yellow
          //       ),),
          //       SizedBox(width: MediaQuery.of(context).size.width*.03,),
          //       Text("Yellow - In quarantine",style: TextStyle(color: Colors.yellow,fontSize: 20,fontWeight: FontWeight.w400),),
          //     ],
          //   ),
          // ),
          // Padding(
          //   padding: const EdgeInsets.symmetric(horizontal: 30,vertical: 20),
          //   child: Row(
          //     children: <Widget>[
          //       Container(width: 15,height: 15,decoration: BoxDecoration(
          //           borderRadius: BorderRadius.circular(15),
          //           color: Colors.grey
          //       ),),
          //       SizedBox(width: MediaQuery.of(context).size.width*.03,),
          //       Text("Gray - Suspected or exposed",style: TextStyle(color: Colors.grey,fontSize: 20,fontWeight: FontWeight.w400),),
          //     ],
          //   ),
          // ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              children: <Widget>[
                Container(width: 15,height: 15,decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Colors.red
                ),),
                SizedBox(width: MediaQuery.of(context).size.width*.03,),
                Text("Red - Confirmed infected",style: TextStyle(color: Colors.red,fontSize: 20,fontWeight: FontWeight.w400),),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
