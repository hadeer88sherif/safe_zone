import 'package:flutter/material.dart';
import 'package:graduation/providers/all_data.dart';
import 'package:graduation/screens/affected_countries.dart';
import 'package:graduation/screens/notifi.dart';
import 'package:graduation/utilities/constant.dart';
import 'package:graduation/widgets/badge.dart';
import 'package:graduation/widgets/counter.dart';
import 'package:graduation/widgets/myheader.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final controller = ScrollController();
  double offset = 0;
  var _isInit = true;
  var _isLoading = false;

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<AllData>(context).fetchAndSetCountriesData().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    controller.addListener(onScroll);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void onScroll() {
    setState(() {
      offset = (controller.hasClients) ? controller.offset : 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    final alldata = Provider.of<AllData>(context, listen: false);
    return Scaffold(
      body: SingleChildScrollView(
        controller: controller,
        child: Column(
          children: <Widget>[
            MyHeader(
              image: "assets/girldoctor.png",
              offset: offset,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Stay at home",
                  style: TextStyle(
                      fontSize: 25,
                      color: Color(0xFF3F6F9F),
                      fontWeight: FontWeight.w700),
                ),
                Badge(
                    child: IconButton(
                      icon: Icon(
                        Icons.notifications,
                        size: 30,
                      ),
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => Notifications()));
                      },
                    ),
                    value: "2")
              ],
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: <Widget>[
                  SizedBox(height: MediaQuery.of(context).size.height * .04),
                  Container(
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(0, 4),
                          blurRadius: 30,
                          color: Color(0xFFB7B7B7).withOpacity(.16),
                        ),
                      ],
                    ),
                    child: _isLoading || alldata == null
                        ? Center(
                            child: CircularProgressIndicator(),
                          )
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Counter(
                                color: kInfectedColor,
                                number: alldata.worldData['cases'].toString(),
                                title: "Infected",
                              ),
                              Counter(
                                color: kDeathColor,
                                number: alldata.worldData['deaths'].toString(),
                                title: "Deaths",
                              ),
                              Counter(
                                color: kRecovercolor,
                                number:
                                    alldata.worldData['recovered'].toString(),
                                title: "Recovered",
                              ),
                            ],
                          ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * .04),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "All affected countries",
                        style: kTitleTextstyle,
                      ),
                      FlatButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AffectedCountries()));
                        },
                        child: Text(
                          "See details",
                          style: TextStyle(
                            color: kPrimaryColor,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * .05,
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(0, 4),
                          blurRadius: 30,
                          color: Color(0xFFB7B7B7).withOpacity(.16),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
