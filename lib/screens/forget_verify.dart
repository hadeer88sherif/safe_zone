import '../screens/Edit_number.dart';
import '../screens/newpass.dart';
import '../widgets/button.dart';
import '../widgets/logo.dart';
import '../widgets/text.dart';
import 'package:flutter/material.dart';


class ForgetVerify extends StatefulWidget {
  @override
  _ForgetVerifyState createState() => _ForgetVerifyState();
}

class _ForgetVerifyState extends State<ForgetVerify> {
 

  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      body:SingleChildScrollView(
         child:
            
              Column(
                children: <Widget>[
                
                 Padding(
                   padding: const EdgeInsets.only(top: 50),
                   child: logo(context),
                 ),
                      
                  Padding(
                    padding:const EdgeInsets.only(top: 30),
                    child: Text('Forget Password',
                        style: TextStyle(
                          fontSize: 30,
                          color:Theme.of(context).accentColor,
                          fontWeight: FontWeight.bold,
                        )),
                  ),
                 
                 
                  Padding(
                    padding: const EdgeInsets.only(top: 30),
                    child: Text('Please Insert The Code That Sent On Your Mobile',
                        style: TextStyle(
                          fontSize: 16,
                          color: Theme.of(context).accentColor,
                        )),
                  ),
                
                  Padding(
                    padding:const EdgeInsets.only(top: 20),
                    child: Text('01025478963',
                        style: TextStyle(
                          fontSize: 16,
                          color: Theme.of(context).accentColor,
                        )),
                  ),
                 
                  Padding(
                    padding: const EdgeInsets.only(top: 30),
                    child: InkWell(onTap: (){Navigator.of(context).push(MaterialPageRoute(builder: (ctx)=>EditNum()));},child: Text('Edit Mobile Number',style: TextStyle(color: Colors.grey),),),
                  ),
                  Container(width: 120,height: 1,color: Colors.grey,),
                  Padding(
                    padding:
                        const EdgeInsets.only( top: 30),
                    child: textField(hintText: 'Verifecation Code', icon:Icon( Icons.lock)),
                  ),
                
                  Padding(
                    padding: const EdgeInsets.only(top: 80),
                    child: InkWell(
                      child: contbutton(text: 'Submit',txtcolor: Colors.white70,color: Theme.of(context).accentColor),
                      onTap: () {Navigator.of(context).push(MaterialPageRoute(builder: (ctx)=>NewPass()));},
                    ),
                  ),
                ],
              )
            
        
      ),
    );
  }
}
