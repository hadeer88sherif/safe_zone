
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../providers/country_data.dart';
import 'package:http/http.dart'as http;
import 'dart:convert';
class AffectedCountries extends StatefulWidget {
  @override
  _AffectedCountriesState createState() => _AffectedCountriesState();
}
class _AffectedCountriesState extends State<AffectedCountries> {
  List<CountryData> _list=[];
  List<CountryData> _search=[];
  var loading =false;
  Future<List> fetchAndSetCountriesData() async {
    setState(() {
      loading =true;
    });
    _list.clear();
    const countryUrl = 'https://corona.lmao.ninja/v2/countries';
    http.Response responseCountry = await http.get(countryUrl);
    final data = json.decode(responseCountry.body);
    if (responseCountry.statusCode==200) {
      for (Map i in data) {
        setState(() {
          _list.add(CountryData.fromJson(i));
          loading=false;
        });
      }
    }
}
  TextEditingController controller =new TextEditingController();
  onSearch(String text)async{
    _search.clear();
    if(text.isEmpty){
      setState(() {});
      return;
    }
    _list.forEach((f){
      if(f.country.contains(text))
        _search.add(f);
    });
    setState(() {});
  }
@override
  void initState() {
   
    super.initState();
    fetchAndSetCountriesData();
  }

  @override
  Widget build(BuildContext context) {
   
    return Scaffold(
      backgroundColor: Colors.grey[200],
        appBar: AppBar(
         title: Text('Affected Countries'),
          backgroundColor: Color(0xFF3F6F9F),),
      body: Container(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(10.0),
              color: Colors.grey[200],
              child: Card(
                child: ListTile(
                  leading: Icon(Icons.search),
                  title: TextField(
                    controller: controller,
                    onChanged: onSearch,
                    textCapitalization: TextCapitalization.sentences,
                    decoration: InputDecoration(
                      hintText: "Search By Country Name",
                      border: InputBorder.none,
                    ),
                  ),
                  trailing: IconButton(icon: Icon(Icons.cancel), onPressed: (){
                    controller.clear();
                    onSearch("");
                  }),
                ),
              ),
            )
            ,loading? Center(
              child: CircularProgressIndicator(),
            )
            :Expanded(
              child: _search.length !=0 || controller.text.isNotEmpty
                  ?ListView.builder(
                  itemCount: _search.length,
                  itemBuilder:(context,i){
                    final b=_search[i];
                    return Container(
                      margin: EdgeInsets.only(right: 20,left: 20,top: 10,bottom: 10),
                      height:  130,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(color: Colors.white,blurRadius: 0, offset: Offset(0 ,2))
                          ]
                      ),
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 30 ,right: 15),
                            child: Container(
                              margin: EdgeInsets.symmetric(horizontal: 10),
                              child: Column(
                                children: <Widget>[
                                  Text(b.country,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12),),
                                  Image.network(b.countryInfo.flag,height: 50,width: 60,),
                                ],
                              ),
                            ),
                          ),
                          Expanded(child: Padding(
                            padding: const EdgeInsets.only(top :20.0),
                            child: Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("Cases : "+b.cases.toString(),
                                    style: TextStyle(fontWeight: FontWeight.bold,color: Colors.red),),
                                  SizedBox(height: 10,),
                                  Text("Active : "+b.active.toString(),
                                    style: TextStyle(fontWeight: FontWeight.bold,color: Colors.blue),),
                                  SizedBox(height: 10,),
                                  Text("Recovered : "+b.recovered.toString(),
                                    style: TextStyle(fontWeight: FontWeight.bold,color: Colors.green),),
                                  SizedBox(height: 10,),
                                  Text("Deathes : "+b.deaths.toString(),
                                    style: TextStyle(fontWeight: FontWeight.bold,color: Colors.grey[800]),),
                                ],
                              ),
                            ),
                          ))
                        ],
                      ),
                    );
                  }
              )
                  :ListView.builder(
                itemCount: _list.length,
                itemBuilder: (ctx, i) {
                  final a= _list[i];
                  return Container(
                    margin: EdgeInsets.only(right: 20,left: 20,top: 10,bottom: 10),
                    height:  130,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(color: Colors.white,blurRadius: 0, offset: Offset(0 ,2))
                        ]
                    ),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 30 ,right: 15),
                          child: Container(
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            child: Column(
                              children: <Widget>[
                                Text(a.country,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12),),
                                Image.network(a.countryInfo.flag,height: 50,width: 60,),
                              ],
                            ),
                          ),
                        ),
                        Expanded(child: Padding(
                          padding: const EdgeInsets.only(top :20.0),
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text("Cases : "+a.cases.toString(),
                                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.red),),
                                SizedBox(height: 10,),
                                Text("Active : "+a.active.toString(),
                                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.blue),),
                                SizedBox(height: 10,),
                                Text("Recovered : "+a.recovered.toString(),
                                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.green),),
                                SizedBox(height: 10,),
                                Text("Deathes : "+a.deaths.toString(),
                                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.grey[800]),),
                              ],
                            ),
                          ),
                        ))
                      ],
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
