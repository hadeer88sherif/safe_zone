import '../screens/forget.dart';

import '../screens/location.dart';
import '../widgets/button.dart';
import '../widgets/logo.dart';
import '../widgets/text.dart';
import 'package:flutter/material.dart';

class Locationsign extends StatelessWidget {
 
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(onPressed: (){Navigator.pop(context);},icon: Icon(Icons.arrow_back_ios),),
        title: Text(
          'Sign UP',
          style: TextStyle(
              color: Colors.white, fontSize: 20, fontWeight: FontWeight.w500),
        ),
      ),
      body: SingleChildScrollView(
              child: Column(
          
          children: <Widget>[
          
            Padding(
              padding: const EdgeInsets.only(top: 80),
              child: logo(context),
            ),
           
            Padding(
              padding: const EdgeInsets.only(top: 30),
              child: textField(hintText: 'Market title',icon:Icon( Icons.person_outline)),
            ),
            textField(t: true,hintText: 'Password',icon:Icon( Icons.lock_outline)),
          
        
        
             Padding(
               padding: const EdgeInsets.only(left: 220),
               child: Column(
                 children: <Widget>[
                   InkWell(onTap: (){Navigator.of(context).push(MaterialPageRoute(builder: (ctx)=>Forget()));},child: Text('Forget Password ?',style: TextStyle(color: Colors.grey),)),
                    Container(width: 110,height: 1,color: Colors.grey,),
                 ],
               ),
             ),
          
          
         
            
            Padding(
              padding: const EdgeInsets.only(top: 30),
              child: InkWell(onTap: (){Navigator.of(context).push(MaterialPageRoute(builder: (ctx)=>Locations()));},child: contbutton(text: 'Submit',txtcolor: Colors.white70,color: Theme.of(context).accentColor)),
            )
          ],
        ),
      ),
    );
  }
}
