import 'package:graduation/screens/generate.dart';

import '../widgets/text.dart';

import 'package:graduation/providers/location_provider.dart';
import 'package:graduation/widgets/logo.dart';
import 'package:provider/provider.dart';

import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Edit extends StatefulWidget {
  static const routname = '/editloc';

  @override
  _EditState createState() => _EditState();
}

class _EditState extends State<Edit> {
  double _latitude;
  double _longitude;
  String _title;

  Future<void> _getCurrentLocation() async {
    final locData = await Location().getLocation();
    setState(() {
      _latitude = locData.latitude;
      _longitude = locData.longitude;
      _title = title;
      marketdata =
          LocationItem(lat: _latitude, long: _longitude, title: _title);
    });
  }

  var marketdata = LocationItem(long: null, lat: null, title: '');

  final cont = TextEditingController();
  final _form = GlobalKey<FormState>();
  String title;

  @override
  Widget build(BuildContext context) {
    final localdata = Provider.of<MarketPlace>(context).zones;

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
        ),
        title: Text("Edit"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 70),
          child: Column(
            children: <Widget>[
              logo(context),
              Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 50),
                child: Text(
                  'Safe Zone',
                  style: TextStyle(fontSize: 16, color: Colors.grey),
                ),
              ),
              Form(
                  key: _form,
                  child: textField(
                    onChange: (String value) {
                      setState(() {
                        title = value;
                      });
                    },
                    hintText: 'Title',
                    initialvalue: localdata[0].title,
                  )),
              SizedBox(
                height: 10,
              ),
              Card(
                margin: EdgeInsets.symmetric(horizontal: 30),
                child: Row(
                  children: <Widget>[
                    SizedBox(
                      width: 15,
                    ),
                    Text(
                      'Edit current location',
                      style: TextStyle(color: Theme.of(context).accentColor),
                    ),
                    Spacer(),
                    IconButton(
                        icon: Icon(Icons.edit),
                        color: Theme.of(context).accentColor,
                        onPressed: () {
                          _getCurrentLocation();
                        })
                  ],
                ),
              ),
              _longitude != null && _latitude != null
                  ? Container(
                      margin: EdgeInsets.symmetric(horizontal: 30),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border:
                              Border.all(color: Theme.of(context).accentColor)),
                      height: 200.0,
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: GoogleMap(
                        initialCameraPosition: CameraPosition(
                          target: LatLng(_latitude, _longitude),
                          zoom: 17.0,
                        ),
                        mapType: MapType.normal,
                      ))
                  : Container(),
              Padding(
                padding: const EdgeInsets.only(top: 25),
                child: InkWell(
                  onTap: () {
                    if (_longitude != null &&
                        _latitude != null &&
                        _title != null) {
                      setState(() {
                        Provider.of<MarketPlace>(context, listen: false)
                            .updateMarket(marketdata);
                      });
                      Navigator.of(context).pushReplacement(
                          MaterialPageRoute(builder: (ctx) => Qr()));
                      print(marketdata.lat);
                      print(marketdata.long);
                      print(marketdata.title);
                    }
                  },
                  child: Container(
                    width: 100,
                    height: 40,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Theme.of(context).accentColor),
                    child: Center(
                      child: Text(
                        'Submit',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
