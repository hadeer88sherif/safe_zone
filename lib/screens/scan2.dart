import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:graduation/widgets/buttom_nav_bar.dart';

class Scan extends StatefulWidget {
  @override
  _ScanState createState() => _ScanState();
}

class _ScanState extends State<Scan> {
  String qrResult = "Not Scanned Yet";
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text("scan"),
        leading: Icon(Icons.arrow_back_ios),
      ),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text('resulte',
                style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold,color: Theme.of(context).accentColor),
                textAlign: TextAlign.center),
            Text(
              qrResult,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18.0,color: Theme.of(context).accentColor),
            ),
            SizedBox(
              height: 20.0,
            ),
            FlatButton(
              padding: EdgeInsets.all(15.0),
              child: Text("Scane QR Code",style: TextStyle(color: Theme.of(context).accentColor),),
              onPressed: () async {
                setState(() {
                  qrResult = ' ';
                });
                var scaning = await BarcodeScanner.scan();
                
                
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => BottomNavBar()));
                
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                  side: BorderSide(color: Theme.of(context).accentColor, width: 1.0)),
            ),
          ],
        ),
      ),
    );
  }
}
