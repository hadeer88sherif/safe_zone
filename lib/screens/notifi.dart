import 'package:flutter/material.dart';


class Notifications extends StatefulWidget {

  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Notification",style: TextStyle(
          fontWeight: FontWeight.bold
        ),
        ),
      ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 25),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(Icons.notifications,size: 70,color: Color(0xFF3F6F9F),),
                  Column(
                    children: <Widget>[
                      Text("Sunday, Jun 21, 2020 at 23:18",style: TextStyle(fontSize: 20),),
                      Text("New Cases",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                      Text("Some new cases are related..",style: TextStyle(fontSize: 20),),
                    ],
                  )
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * .1,
                child: Divider(
                  color: Colors.black26,
                  height: 100,
                  indent: 10,
                  endIndent: 10,
                  thickness: 2,
                ),
              ),
              Row(
                children: <Widget>[
                  Icon(Icons.notifications,size: 70,color: Color(0xFF3F6F9F),),
                  Column(
                    children: <Widget>[
                      Text("Friday, Jun 12, 2020 at 17:03",style: TextStyle(fontSize: 20),),
                      Text("New Cases",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                      Text("Some new cases are related..",style: TextStyle(fontSize: 20),),
                    ],
                  )
                ],
              ),
            ],
          ),
        ));
  }
}
