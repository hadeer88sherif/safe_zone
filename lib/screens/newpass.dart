import '../screens/passsuccess.dart';
import '../widgets/button.dart';
import '../widgets/logo.dart';
import '../widgets/text.dart';
import 'package:flutter/material.dart';

class NewPass extends StatefulWidget {
  @override
  _NewPassState createState() => _NewPassState();
}

class _NewPassState extends State<NewPass> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: 
          
          Column(
            children: <Widget>[
             
              Padding(
                padding: const EdgeInsets.only(top: 70),
                child: logo(context),
              ),
             
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: Text('Forget Password',
                    style: TextStyle(
                      fontSize: 30,
                      color: Theme.of(context).accentColor,
                      fontWeight: FontWeight.bold,
                    )),
              ),
             
              Padding(
                padding:const EdgeInsets.only(top: 30),
                child: Text('Please Insert The Code That Sent On Your Mobile',
                    style: TextStyle(
                        fontSize: 16, color: Theme.of(context).accentColor)),
              ),
             
              Padding(
                padding: const EdgeInsets.only(top: 60),
                child: textField(
                    hintText: ' New Password', icon: Icon(Icons.lock)),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10,bottom: 80),
                child: textField(
                    hintText: 'confirm New Password',
                    icon: Icon(Icons.lock)),
              ),
             
              InkWell(
                child: contbutton(
                    text: 'Submit',
                    txtcolor: Colors.white70,
                    color: Theme.of(context).accentColor),
                onTap: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (ctx) => PassSuccess()));
                },
              ),
            ],
          ),
       
    );
  }
}
