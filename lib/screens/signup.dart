


import 'package:graduation/screens/textdetection.dart';


import 'package:flutter/material.dart';

import '../widgets/button.dart';
import '../widgets/text.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  bool valu1 = false;

 final _form=GlobalKey<FormState>();



  @override
  Widget build(BuildContext context) {
   
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back_ios),
        ),
        title: Text(
          'SignUp',
          style: TextStyle(
              color: Colors.white, fontSize: 20, fontWeight: FontWeight.w500),
        ),
      ),
      body: Form(
        key: _form,
              child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 50),
              child: Container(
                  width: 100,
                  height: 100,
                  child: Image.asset(
                    'assets/logo.png',
                  )),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 30),
              child:  textField(textInputType: TextInputType.phone,hintText: 'Phone', icon: Icon(Icons.phone)),
            ),
            textField(t: true,hintText: 'password', icon: Icon(Icons.lock_outline)),
            textField(t: true,
                hintText: 'confirm password', icon: Icon(Icons.lock_outline)),
          
       
                  
            Padding(
              padding: const EdgeInsets.only(top: 15, left: 15),
              child: Row(
                children: <Widget>[
                  Theme(
                    data: ThemeData(
                        unselectedWidgetColor: Theme.of(context).accentColor),
                    child: Checkbox(
                        value: valu1,
                        activeColor: Theme.of(context).accentColor,
                        onChanged: (bool value) {
                          setState(() {
                            valu1 = value;
                          });
                        }),
                  ),
                  Text(
                    'I Agree To The Termes And Conditions',
                    style: TextStyle(color: Theme.of(context).accentColor),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 90, vertical: 30),
              child: InkWell(
                child: contbutton(
                    text: 'Next', txtcolor: Theme.of(context).accentColor),
                onTap: () {

                  if(_form.currentState.validate()){
                     Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => MyHomePage()));
                  }
                  else{return;}
                  
                 
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
