import 'dart:async';
import 'dart:convert';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:graduation/widgets/button.dart';
import 'package:http/http.dart' as http;
import 'package:graduation/widgets/buttom_nav_bar.dart';
import 'package:graduation/widgets/text.dart';
import 'package:location/location.dart';

import 'package:flutter/material.dart';

class AddNewCase extends StatefulWidget {
  @override
  _AddNewCaseState createState() => _AddNewCaseState();
}

class _AddNewCaseState extends State<AddNewCase> {
  String name;
  String ssn;
  int phone;

  final _form = GlobalKey<FormState>();
  double latitude;
  double longitude;

  Future<void> newcase() async {
    final url = 'https://localhost:44346/api/krona/newcase';
    await http.post(url,
        body: json.encode({
          "NC_SSN": ssn,
          "Name": name,
          "Longitude": longitude,
          "Phone": phone,
          "Latitude": latitude
        }));
    print(latitude);
  }

  Future<void> _getCurrentLocation() async {
    final locData = await Location().getLocation();
    setState(() {
      latitude = locData.latitude;
      longitude = locData.longitude;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        leading: Icon(Icons.arrow_back_ios),
        backgroundColor: getColorFromHex('#FF3F6F9F'),
        title: Text("Add New Case"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 50),
          child: Form(
            key: _form,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 20, bottom: 30),
                  child: Container(
                    height: MediaQuery.of(context).size.height * .2,
                    width: MediaQuery.of(context).size.width * .4,
                    child: Image.asset("assets/logo.png"),
                  ),
                ),
                textField(
                  hintText: 'Name',
                  icon: Icon(Icons.person_outline),
                  onChange: (String value) {
                    setState(() {
                      name =value;
                    });
                  },
                ),
                textField(
                    hintText: 'SSN',
                    icon: Icon(Icons.card_membership),
                   onChange: (String value){
                      setState(() {
                        ssn=value;
                      });
                   },),
                textField(
                    hintText: 'phone',
                    icon: Icon(Icons.phone_iphone),
                    onChange: (String value){
                      setState(() {
                        phone=int.parse(value);
                      });
                    },),
                SizedBox(
                  height: MediaQuery.of(context).size.height * .04,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    longitude == null && latitude == null
                        ? FlatButton.icon(
                            onPressed: _getCurrentLocation,
                            icon: Icon(
                              Icons.location_on,
                              color: getColorFromHex('#3F6F9F'),
                            ),
                            label: Text(
                              "Chosse Current Location ",
                              style:
                                  TextStyle(color: getColorFromHex('#3F6F9F')),
                            ))
                        : Container(),
                    Container(
                      height: MediaQuery.of(context).size.height * .4,
                      width: double.infinity,
                      child: longitude == null && latitude == null
                          ? Text("")
                          : SingleChildScrollView(
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 30),
                                    height: 200,
                                    width: 400,
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            width: 1,
                                            color:
                                                Theme.of(context).accentColor),
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    child: GoogleMap(
                                      initialCameraPosition: CameraPosition(
                                        target: LatLng(latitude, longitude),
                                        zoom: 17.0,
                                      ),
                                      mapType: MapType.normal,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 80,
                                        right: 80,
                                        top: 25,
                                        bottom: 20),
                                    child: Container(
                                      height: 40,
                                      width: MediaQuery.of(context).size.width,
                                      decoration: BoxDecoration(
                                          color: Colors.transparent,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15)),
                                          border: Border.all(
                                              color: getColorFromHex(
                                                  '#FF3F6F9F'))),
                                      child: FlatButton(
                                        child: Text(
                                          'Submit',
                                          style: TextStyle(
                                            color: getColorFromHex('#3F6F9F'),
                                          ),
                                        ),
                                        onPressed: () {
                                          newcase();

                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (ctx) =>
                                                      BottomNavBar()));
                                        },
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

