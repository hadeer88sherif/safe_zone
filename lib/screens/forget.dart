import '../screens/forget_verify.dart';
import '../widgets/button.dart';
import '../widgets/logo.dart';
import '../widgets/text.dart';
import 'package:flutter/material.dart';



class Forget extends StatefulWidget {
  @override
  _ForgetState createState() => _ForgetState();
}

class _ForgetState extends State<Forget> {
  

  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
   
      body:SingleChildScrollView(child: 
        
              
              Column(
                children: <Widget>[
                 
                Padding(
                  padding:const EdgeInsets.only(top: 70),
                  child: logo(context),
                ),
                     
                  Padding(
                    padding: const EdgeInsets.only(top: 30),
                    child: Text('Forget Password',
                        style: TextStyle(
                          fontSize: 25,
                          color:Theme.of(context).accentColor,
                          fontWeight: FontWeight.bold,
                        )),
                  ),
                  
                  Padding(
                    padding: const EdgeInsets.only(top: 30),
                    child: Text('Please Insert Your Mobile Number',
                        style: TextStyle(
                          fontSize: 16,
                          color: Theme.of(context).accentColor
                        )),
                  ),
                 
                
                  Padding(
                    padding:
                        const EdgeInsets.only( top: 70),
                    child: textField(hintText: 'Mobile Number', icon:Icon( Icons.call)),
                  ),
                
                  Padding(
                    padding:const EdgeInsets.only(top: 80),
                    child: InkWell(
                      child: contbutton(text: 'Submit',txtcolor: Colors.white70,color:Theme.of(context).accentColor),
                      onTap: () {Navigator.of(context).push(MaterialPageRoute(builder: (ctx)=>ForgetVerify()));},
                    ),
                  ),
                ],
              )
        
      ),
    );
  }
}
