import '../screens/Login.dart';
import '../screens/signup.dart';
import '../widgets/button.dart';

import 'package:flutter/material.dart';

import 'location_signup.dart';

class Register extends StatefulWidget {
  
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  bool color1=false;
  bool color=false;
 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
              child: Padding(
          padding: const EdgeInsets.only( top: 80),
          child: Column(
            
            children: <Widget>[
             
                  
                    Image.asset(
                      'assets/logo.png',
                      width: MediaQuery.of(context).size.width * .4,
                    height: MediaQuery.of(context).size.height * .2,),
                  
                  
                 
              Row(
                
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  
                  Text(
                    'Safe Z',
                    style: TextStyle(
                        fontSize: 20, color: Theme.of(context).accentColor),
                  ),
                  Container(
                      width: 30,
                      height: 30,
                      child: Image.asset(
                        'assets/o.png',
                        fit: BoxFit.cover,
                      )),
                  Text('ne',
                      style: TextStyle(
                          fontSize: 20, color: Theme.of(context).accentColor))
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 190),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    InkWell(
                      onTap: () {setState(() {
                        color=true;
                        color1=false;
                      });},
                      child:color? Container(
                          child: Center(
                            child: Container(
                              width: 7,
                              height: 7,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white),
                            ),
                          ),
                          width: 20,
                          height: 20,
                          decoration: BoxDecoration(
                              color: Theme.of(context).accentColor,
                              borderRadius: BorderRadius.circular(90)),
                        ):Container(
                          width: 20,
                          height: 20,
                          decoration: BoxDecoration(
                            border:
                                Border.all(color: Theme.of(context).accentColor),
                            borderRadius: BorderRadius.circular(90),
                          )),
                    ),
                    Text(' User',
                        style: TextStyle(
                            fontSize: 16, color: Theme.of(context).accentColor)),
                            
                    Padding(
                      padding: const EdgeInsets.only(left: 120),
                      child: InkWell(
                        onTap: () {setState(() {
                          color1=true;
                          color=false;
                        });},
                        child:color1? Container(
                          child: Center(
                            child: Container(
                              width: 7,
                              height: 7,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white),
                            ),
                          ),
                          width: 20,
                          height: 20,
                          decoration: BoxDecoration(
                              color: Theme.of(context).accentColor,
                              borderRadius: BorderRadius.circular(90)),
                        ):Container(
                          width: 20,
                          height: 20,
                          decoration: BoxDecoration(
                            border:
                                Border.all(color: Theme.of(context).accentColor),
                            borderRadius: BorderRadius.circular(90),
                          )),
                      ),
                    ),
                    Text(' zone',
                        style: TextStyle(
                            fontSize: 16, color: Theme.of(context).accentColor))
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 50,bottom: 30),
                child: InkWell(
                  onTap: () {
                   color1? Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) => Locationsign())) : Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) => SignUp()));
                  },
                  child:contbutton(
                      text: 'SignUp', txtcolor: Theme.of(context).accentColor),
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) => Login()));
                },
                child: contbutton(
                  text: 'LOGIN',
                  color: Theme.of(context).accentColor,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
