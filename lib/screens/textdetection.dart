import 'dart:io';
import 'package:graduation/widgets/logo.dart';
import 'package:location/location.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';
import 'package:graduation/screens/verfication.dart';
import 'package:graduation/widgets/button.dart';

import 'package:image_picker/image_picker.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  File pickedImage;
  String _items;

  bool isImageLoaded = false;
  bool isdone = false;
  void pickImage() async {
    var tempStore = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      pickedImage = tempStore;
      isImageLoaded = true;
    });
  }

  void readText() async {
    FirebaseVisionImage ourImage = FirebaseVisionImage.fromFile(pickedImage);
    TextRecognizer recognizeText = FirebaseVision.instance.textRecognizer();
    VisionText readText = await recognizeText.processImage(ourImage);

    for (TextBlock block in readText.blocks) {
      for (TextLine line in block.lines) {
        for (TextElement word in line.elements) {
          _items = word.text;
          print(_items);
        }
      }
    }
    setState(() {
      isImageLoaded = false;
      isdone = true;
    });
  }

  double latitude;
  double longitude;

  Future<void> _getCurrentLocation() async {
    final locData = await Location().getLocation();
    setState(() {
      latitude = locData.latitude;
      longitude = locData.longitude;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
     
        children: <Widget>[
      longitude == null && latitude == null?  Padding(
         padding: const EdgeInsets.only(top: 150),
         child: logo(context),
       ):Container(),
          longitude != null && latitude != null
              ? Container(
                  height: 400.0,
                  width: double.infinity,
                  alignment: Alignment.center,
                  child: GoogleMap(
                    initialCameraPosition: CameraPosition(
                      target: LatLng(latitude, longitude),
                      zoom: 17.0,
                    ),
                    mapType: MapType.normal,
                  ))
              : Padding(
                  padding: const EdgeInsets.only(top: 100),
                  child: InkWell(
                    onTap: () {
                      _getCurrentLocation();
                    },
                    child: contbutton(
                        icondata: Icon(Icons.add_location),
                        text: 'Current Location',
                        txtcolor: Theme.of(context).accentColor),
                  ),
                ),
          SizedBox(
            height: 10,
          ),
          !isdone
              ? InkWell(
                  onTap: () {
                    pickImage();
                  },
                  child: contbutton(
                      icondata: Icon(Icons.camera),
                      text: 'Scan Your Card',
                      txtcolor: Theme.of(context).accentColor),
                )
              : Container(),
          isImageLoaded
              ? Center(
                  child: Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      height: 200.0,
                      width: 200.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          image: DecorationImage(
                              image: FileImage(pickedImage),
                              fit: BoxFit.cover))),
                )
              : Container(),
          SizedBox(height: 10.0),
          !isdone
              ? InkWell(
                  onTap: () {
                    readText();
                  },
                  child: contbutton(
                      icondata: Icon(Icons.receipt),
                      text: 'Detect Idinitity Code',
                      txtcolor: Theme.of(context).accentColor),
                )
              : Padding(
                padding: const EdgeInsets.symmetric(horizontal: 50),
                child: Card(

                   
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 30,horizontal: 20),
                      child: Row(
                        children: <Widget>[
                        
                          Text(
                            ' Your Idinitity Code : ',
                            style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16,color: Theme.of(context).accentColor),
                          ),
                          Text(_items,
                              style:
                                  TextStyle(color: Theme.of(context).accentColor)),
                        ],
                      ),
                    ),
                    elevation: 7,
                  ),
              ),
          SizedBox(
            height: 100,
          ),
        isdone?  InkWell(
            onTap: (
          
            ) {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (ctx) => Verfication()));
            },
            child: contbutton(
                text: 'Submit', txtcolor: Theme.of(context).accentColor),
          ):Container()
        ],
      ),
    ));
  }
}
