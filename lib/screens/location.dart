
import 'package:graduation/screens/generate.dart';

import '../widgets/logo.dart';
import '../widgets/text.dart';
import 'package:provider/provider.dart';
import '../providers/location_provider.dart';
import 'package:flutter/material.dart';
import '../widgets/button.dart';

import 'package:location/location.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Locations extends StatefulWidget {
  @override
  _LocationsState createState() => _LocationsState();
}

class _LocationsState extends State<Locations> {
  double _latitude;
  double _longitude;
  bool generate = false;

  Future<void> _currentLocation() async {
    final locData = await Location().getLocation();
    setState(() {
      _latitude = locData.latitude;
      _longitude = locData.longitude;
      marketdata = LocationItem(lat: _latitude, long: _longitude, title: title);
    });
  }

  final _form = GlobalKey<FormState>();
  var marketdata = LocationItem(long: null, lat: null, title: '');
  String title;

  @override
  Widget build(BuildContext context) {
    final market = Provider.of<MarketPlace>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Location',
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.w500, color: Colors.white),
        ),
      ),
      body:
//      generate
//          ? Center(
//              child: Container(
//              width: 300,
//              height: 300,
//              child: Image.asset('assets/qrcode.jpg'),
//            )) :
          SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 50),
          child: Column(
            children: <Widget>[
              logo(context),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(
                  'Safe Zone',
                  style: TextStyle(fontSize: 16, color: Colors.grey),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 50),
                child: Form(
                    key: _form,
                    child: textField(
                      onChange: (String value) {
                        setState(() {
                          title = value;
                        });
                      },
                      hintText: 'Title',
                    )),
              ),
              _longitude == null && _latitude == null
                  ? Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: InkWell(
                        onTap: () {
                          _currentLocation();
                        },
                        child: contbutton(
                            icondata: Icon(Icons.add_location),
                            text: 'Current Location',
                            txtcolor: Theme.of(context).accentColor),
                      ),
                    )
                  : Container(),
              _longitude != null && _latitude != null
                  ? Container(
                      margin:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                      height: 200.0,
                      width: 400.0,
                      decoration: BoxDecoration(
                          border: Border.all(
                              width: 1, color: Theme.of(context).accentColor),
                          borderRadius: BorderRadius.circular(10)),
                      alignment: Alignment.center,
                      child: GoogleMap(
                        initialCameraPosition: CameraPosition(
                          target: LatLng(_latitude, _longitude),
                          zoom: 17.0,
                        ),
                        mapType: MapType.normal,
                      ))
                  : Container(),
              Padding(
                padding: const EdgeInsets.only(top: 25),
                child: InkWell(
                  onTap: () {
                    market.addMarket(marketdata);
                    if (_latitude != null &&
                        _longitude != null &&
                        title != null) {
                      Navigator.of(context).pushReplacement(
                          MaterialPageRoute(builder: (ctx) => Qr()));
                    }

                    print(marketdata.lat);
                    print(marketdata.long);
                    print(marketdata.title);
                  },
                  child: contbutton(
                      text: 'Generate',
                      txtcolor: Colors.white70,
                      color: Theme.of(context).accentColor),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
