import '../screens/Edit_number.dart';
import '../screens/success.dart';

import '../widgets/text.dart';
import '../widgets/button.dart';

import 'package:flutter/material.dart';

class Verfication extends StatefulWidget {
  @override
  _VerficationState createState() => _VerficationState();
}

class _VerficationState extends State<Verfication> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(child: 
              Column(
                children: <Widget>[
                
                  Padding(
                    padding: const EdgeInsets.only(top: 70,bottom: 30),
                    child: Container(
                        width: 80,
                        height: 80,
                        child: Image.asset(
                          'assets/logo.png',
                          fit: BoxFit.cover,
                        )),
                  ),
                 
                  Text('Verify Your Account',
                      style: TextStyle(
                        fontSize: 30,
                        color:Theme.of(context).accentColor,
                        fontWeight: FontWeight.bold,
                      )),
                
                  Padding(
                    padding:const EdgeInsets.only(top: 50,bottom: 20),
                    child: Text('Please Insert The Code That Sent On Your Mobile',
                        style: TextStyle(
                          fontSize: 16,
                          color:Theme.of(context).accentColor,
                        )),
                  ),
                 
                  Text('01025478963',
                      style: TextStyle(
                        fontSize: 16,
                        color: Theme.of(context).accentColor,
                      )),
                  
                  Padding(
                    padding: const EdgeInsets.only(top: 40),
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context)
                            .push(MaterialPageRoute(builder: (ctx) => EditNum()));
                      },
                      child: Text(
                        'Edit Mobile Number',
                        style: TextStyle(color: Colors.grey),
                      ),
                    ),
                  ),
                  Container(
                    width: 120,
                    height: 1,
                    color: Colors.grey,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 60,bottom: 100),
                    child: textField(
                        hintText: 'Verifecation Code', icon: Icon(Icons.lock)),
                  ),
                  
                  InkWell(
                    child: contbutton(
                      text: 'Submit',
                      txtcolor: Colors.white70,
                      color:Theme.of(context).accentColor
                    ),
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (ctx) => Success()));
                    },
                  ),
                ],
              )
           
      ),
    );
  }
}
